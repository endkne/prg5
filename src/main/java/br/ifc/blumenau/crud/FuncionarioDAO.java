package br.ifc.blumenau.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author endrigo
 */
public class FuncionarioDAO extends BaseDAO {


    public FuncionarioDTO buscarPorId(long id) {

        FuncionarioDTO funcionario = null;

        try {
            Connection conexao = getConnection();
            String sql = "select id, nome, idade, telefone from funcionario where id = ?";
            PreparedStatement ps = conexao.prepareStatement(sql);
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                funcionario = new FuncionarioDTO(rs.getLong("id"),
                        rs.getString("nome"), rs.getInt("idade"), rs.getString("telefone"));
            }
            ps.close();
            conexao.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionario;
    }

    public static void main(String[] args) {

        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        System.out.println("Nome do funcionário buscado: " + funcionarioDAO.buscarPorId(1).getIdade());
    }
}
