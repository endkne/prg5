package br.ifc.blumenau.crud;

import java.sql.Connection;

/**
 *
 * @author endrigo
 */
public abstract class BaseDAO {
    
    protected Connection getConnection() {
        return Conexao.getConnection();
    }
    
}
