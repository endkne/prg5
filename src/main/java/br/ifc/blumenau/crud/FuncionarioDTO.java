package br.ifc.blumenau.crud;


/**
 *
 * @author endrigo
 */
public class FuncionarioDTO {
    
    private long id;
    private String nome;
    private int idade;
    private String telefone;

    public FuncionarioDTO() {
    }

    public FuncionarioDTO(String nome) {
        this.nome = nome;
    }
    

    public FuncionarioDTO(long id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    
    

    public FuncionarioDTO(long id, String nome, int idade, String telefone) {
        this.id = id;
        this.nome = nome;
        this.idade = idade;
        this.telefone = telefone;
    }
    
    

    public FuncionarioDTO(String nome, int idade, String telefone) {
        this.nome = nome;
        this.idade = idade;
        this.telefone = telefone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    
    
    
}
    
