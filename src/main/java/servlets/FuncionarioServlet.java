package servlets;

import br.ifc.blumenau.crud.FuncionarioDAO;
import br.ifc.blumenau.crud.FuncionarioDTO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author endrigo
 */
@WebServlet(urlPatterns = {"/FuncionarioServlet"})
public class FuncionarioServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String idLong = request.getParameter("id");

        long id = Long.parseLong(idLong);

        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        request.setAttribute("funcionario", funcionarioDAO.buscarPorId(id));
        request.getRequestDispatcher("listarFuncionario.jsp").forward(request, response);

    }
}
