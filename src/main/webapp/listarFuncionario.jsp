<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <title>Funcionários</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/crud.css" rel="stylesheet">
        
    </head>
    <body>
        <div><h5>CRUD > Funcionários</h5></div>
        <table> <table border="1"> 
            
            <TR>
                <TD>ID </TD>
                <TD>FUNCIONÁRIO </TD>
                <TD>IDADE </TD>
                <TD>TELEFONE </TD>
            </TR>
            <tr>    
                <td>${funcionario.id}</td>"
                <td>${funcionario.nome}</td>
                <td>${funcionario.idade}</td>
                <td>${funcionario.telefone}</td>
            </tr>
    </body>
</html>
